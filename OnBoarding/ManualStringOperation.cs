﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OnBoarding
{
    class ManualStringOperation : StringOperations
    {
        public bool CompareString(string str1, string str2)
        {
           // Console.WriteLine("Using User Defined Method");
            bool check = false;
            int str1Length = str1.Length;
            int str2Length = str2.Length;
            if (str1Length != str2Length)
                check = false;
            else
                for (int count = 0; count < str1Length; count++)
                {
                    if (str1[count].Equals(str2[count]))
                        check = true;
                    else
                    {
                        check = false;
                        break;
                    }
                }
            Console.WriteLine("Using User Defined Method");
            return check;
        }
        public int FindCharCountInString(string str, char c)
        {
            int totalCharcount = 0;
            int strLength = str.Length;
            for (int count = 0; count < strLength; count++)
            {
                if (str[count] == c)
                {
                    totalCharcount++;
                }
            }

            return totalCharcount;
        }
    }
}
