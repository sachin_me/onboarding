﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OnBoarding
{
    class HelloWorld
    {
        
        public static void Main(string[] args)
        {
            StringOperations operation = new StringOperations();
            ManualStringOperation userOperation = new ManualStringOperation();

            Console.WriteLine("Hello World!");
            // Debug.WriteLine("Hello World");

            bool resullt1 = operation.CompareString("abc", "abc");
            bool resullt2 = userOperation.CompareString("abc", "abc");
            int totalCharcount = userOperation.FindCharCountInString("abdf", 'c');

            Console.WriteLine(resullt1);
            Console.WriteLine(resullt2);
            Console.WriteLine(totalCharcount);

            //Debug.WriteLine(resullt);
        }    
    }
}
